
package hangman;

import eg.edu.alexu.csd.datastructure.hangman.IHangman;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class Hangman implements IHangman
{
    private static final String FILENAME = "Friends.txt";
    private static String words[] = new String[100];
    private static int nWords = 0;
    private static int maxTrials = 10;
    private static int trials = 0;
    private static String secretWord;        
    private static char currentWord[];
    private static char wrongGuesses[];

    public static void main(String[] arg) 
    {
        System.out.println("Welcome to F.R.I.E.N.D.S hangman game\n");
        Hangman h = new Hangman();
        h.setDictionary();
        Scanner in  = new Scanner(System.in);
        do{
            int option;
            do{
                System.out.println("1) Start Game\n2) Set Maximum Wrong Guesses (current is " + maxTrials +")\n3) Exit");
                option = in.nextInt();
            }while(option!=1 && option!=2 && option!=3);

            if(option == 2){
                int max;
                do{
                    System.out.print("Maximum = ");
                }while((max = in.nextInt()) < 1);
                h.setMaxWrongGuesses(max);
            }
            
            if(option == 3)
                System.exit(0);

            h.initializeGame();
            System.out.println(" ");
            System.out.println(currentWord);

            while(trials <= maxTrials)
            {
                h.guess(in.next().toUpperCase().charAt(0));
                System.out.println(currentWord);
                System.out.println("Wrong Guesses: " + Arrays.toString(wrongGuesses) + "  Remaining = " + (maxTrials-trials));
                if(gameWon())
                {   
                    System.out.println("\nYou Won!");
                    System.out.println(currentWord);
                    break;
                }
                if(trials == maxTrials)
                {
                    System.out.println("\nYou lost!");
                    System.out.println(secretWord);
                    break;
                }
            }
            
            System.out.println("\nNew Game? (Press any charecter to proceed. Press 0 to exit)");
        
        }while(in.next().charAt(0)!='0');
    }
    
    public void initializeGame()
    {
        secretWord = selectRandomSecretWord();
        currentWord = new char[secretWord.length()];
        hideWord();
        wrongGuesses = new char[maxTrials];
        trials = 0;
    }
    
    @Override
    public void setDictionary() 
    {
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
                            words[nWords] = sCurrentLine;
                            nWords++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    @Override
    public String selectRandomSecretWord() 
    {
        Random rand = new Random();
        int  n = rand.nextInt(nWords-1);
        return words[n].toUpperCase();
    }

    public static void hideWord()
    {
        
        for(int i=0; i<secretWord.length(); i++)
        {
            if(secretWord.charAt(i) == ' ')
                currentWord[i] = ' ';
            
            else
                currentWord[i] = '-';
        }
    }
    
    @Override
    public String guess(Character c) //throws Exception 
    {
        boolean success = false;
        for(int i=0; i<secretWord.length(); i++)
        {
            if(secretWord.charAt(i) == c)
            {
                success = true;
                if(currentWord[i] == '-')                
                    currentWord[i] = c;
            }
        }
        int i;
        for(i=0; i<trials; i++)
            if(wrongGuesses[i]==c)
                break;
        
        if(i == trials && !success)
        {
            wrongGuesses[trials] = c;
            trials++;

        }
        return currentWord.toString();
    }
    
    public static boolean gameWon()
    {
        for(int i=0; i<secretWord.length(); i++)
        {
            if(currentWord[i] == '-')
                return false;
                        
        }
        return true;
    }
    
    @Override
    public void setMaxWrongGuesses(Integer max) 
    {
       maxTrials = max;
    }
    
}